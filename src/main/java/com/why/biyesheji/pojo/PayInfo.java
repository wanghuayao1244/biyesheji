package com.why.biyesheji.pojo;

import java.util.Date;

public class PayInfo {
    private Integer id;

    private Integer userId;

    private Long orderNo;

    private Integer payPlaftform;

    private String plaftformNumber;

    private String plaftformStatus;

    private Date createTime;

    private Date updateTime;

    public PayInfo(Integer id, Integer userId, Long orderNo, Integer payPlaftform, String plaftformNumber, String plaftformStatus, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.orderNo = orderNo;
        this.payPlaftform = payPlaftform;
        this.plaftformNumber = plaftformNumber;
        this.plaftformStatus = plaftformStatus;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public PayInfo() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getPayPlaftform() {
        return payPlaftform;
    }

    public void setPayPlaftform(Integer payPlaftform) {
        this.payPlaftform = payPlaftform;
    }

    public String getPlaftformNumber() {
        return plaftformNumber;
    }

    public void setPlaftformNumber(String plaftformNumber) {
        this.plaftformNumber = plaftformNumber == null ? null : plaftformNumber.trim();
    }

    public String getPlaftformStatus() {
        return plaftformStatus;
    }

    public void setPlaftformStatus(String plaftformStatus) {
        this.plaftformStatus = plaftformStatus == null ? null : plaftformStatus.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}