package com.why.biyesheji.util;

import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * 使用FTP服务器，集群的话，就可以统一管理文件和图片了。
 * List<File> fileList，为批量上传设置
 * Created by 华耀 王 on 2020/3/23
 **/

public class FTPUtil {
    private static final Logger logger = LoggerFactory.getLogger(FTPUtil.class);

    private static String ftpIp = PropertiesUtil.getProperty("ftp.server.ip");
    private static String ftpUser = PropertiesUtil.getProperty("ftp.user");
    private static String ftpPass = PropertiesUtil.getProperty("ftp.pass");

    private String ip;
    private int port;
    private String user;
    private String pwd;
    private FTPClient ftpClient;

    public FTPUtil(String ip, int port, String user, String pwd){
        this.ip = ip;
        this.port = port;
        this.user = user;
        this.pwd = pwd;
    }
    public static boolean uploadFile(List<File> fileList) throws IOException {
        FTPUtil ftpUtil = new FTPUtil(ftpIp,21,ftpUser,ftpPass);
        logger.info("开始连接ftp服务器");
        boolean result = ftpUtil.uploadFile("img",fileList);
        logger.info("开始连接ftp服务器，结束上传，上传结果：>"+result);
        return result;
    }

    public static boolean delUploadFile(String fileName) throws IOException {
        FTPUtil ftpUtil = new FTPUtil(ftpIp,21,ftpUser,ftpPass);
        logger.info("开始连接ftp服务器");
        boolean result = ftpUtil.delUploadFile("img",fileName);
        logger.info("开始连接ftp服务器，结束删除操作，操作结果：{}");
        return result;
    }

    //上传图片拆出来的一个封装
    private boolean uploadFile(String remotepath,List<File> fileList) throws IOException {
        boolean uploaded = true;
        FileInputStream  fis = null;
        //连接FTP服务器
        if (connectServer(this.ip,this.port,this.user,this.pwd)){
            try {
                ftpClient.changeWorkingDirectory(remotepath);
                ftpClient.setBufferSize(1024);
                ftpClient.setControlEncoding("UTF-8");
                ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);//设置成这个防止乱码的问题
                ftpClient.enterLocalPassiveMode();
                for (File fileItem : fileList) {
                    fis = new FileInputStream(fileItem);
                    ftpClient.storeFile(fileItem.getName(),fis);
                }
            } catch (IOException e) {
                logger.error("上传文件异常",e);
                uploaded = false;
            }finally {
                fis.close();
                ftpClient.disconnect();
            }
        }
        return uploaded;
    }

    //拆出来的一个封装
    private boolean delUploadFile(String remotepath,String fileName) throws IOException {
        boolean uploaded = true;
        //连接FTP服务器
        if (connectServer(this.ip,this.port,this.user,this.pwd)){
            try {
                ftpClient.changeWorkingDirectory(remotepath);//工作路径切换到remotepath
                ftpClient.setBufferSize(1024);//设置1M缓冲
                ftpClient.setControlEncoding("UTF-8");//设置编码为UTF-8
                ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);//设置成这个防止乱码的问题
                ftpClient.enterLocalPassiveMode();//这个方法的意思就是每次数据连接之前，ftp client告诉ftp server开通一个端口来传输数据
                System.out.println(fileName);
                boolean test = ftpClient.deleteFile(fileName);
                System.out.println(test+"图片已删除！");
            } catch (IOException e) {
                logger.error("删除文件异常",e);
                uploaded = false;
            }finally {
                ftpClient.disconnect();
            }
        }
        return uploaded;
    }

    //封装方法
    private boolean connectServer(String ip,int port,String user,String pwd){
        boolean isSuccess = false;
        ftpClient = new FTPClient();
        try {
            ftpClient.connect(ip);
            isSuccess = ftpClient.login(user,pwd);
        } catch (IOException e) {
            logger.error("连接FTP服务器异常",e);
        }
        return isSuccess;
    }


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public FTPClient getFtpClient() {
        return ftpClient;
    }

    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }
}
