package com.why.biyesheji.common;

/**
 * Created by 华耀 王 on 2020/10/15
 **/

public enum ServiceResultEnum {
    ERROR("error"),

    SUCCESS("success"),

    LOGIN_NAME_NULL("请输入登录名！"),

    LOGIN_NAME_NOTEXIST("用户不存在"),

    REGISTER_NAME_EXIST("用户已存在"),

    LOGIN_PASSWORD_NULL("请输入密码！"),

    LOGIN_VERIFY_CODE_NULL("请输入验证码！"),

    LOGIN_VERIFY_CODE_ERROR("验证码错误！"),

    LOGIN_PASSWORD_ERROR("密码错误"),
    ;

    private String result;

    ServiceResultEnum(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
