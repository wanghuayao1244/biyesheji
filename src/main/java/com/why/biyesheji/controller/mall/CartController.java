package com.why.biyesheji.controller.mall;

import com.why.biyesheji.common.Constants;
import com.why.biyesheji.common.ServiceResultEnum;
import com.why.biyesheji.pojo.Cart;
import com.why.biyesheji.pojo.User;
import com.why.biyesheji.service.ICartService;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.util.ResultGenerator;
import com.why.biyesheji.vo.CartListVO;
import com.why.biyesheji.vo.MallUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 华耀 王 on 2020/12/22
 **/
@Controller
public class CartController {

    @Autowired
    private ICartService iCartService;

    @GetMapping({"/cart_list","/cart_list.html"})
    public String cartList(HttpSession session, Model model){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return Constants.NEED_LOGIN;
        }
        CartListVO cartListVO = iCartService.cartList(currentUser.getId());
        model.addAttribute("cartListVO",cartListVO);
        return "mall/cart";
    }

    @PutMapping("/cart_select_all")
    @ResponseBody
    public Result selectAllProduct(HttpSession session){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }

        return iCartService.selectOrUnSelect(currentUser.getId(),null,Constants.Cart.CHECK);
    }

    @PutMapping("/cart_un_select_all")
    @ResponseBody
    public Result unSelectAllProduct(HttpSession session){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iCartService.selectOrUnSelect(currentUser.getId(),null,Constants.Cart.UN_CHECK);
    }

    @PutMapping("/cart_select")
    @ResponseBody
    public Result selectProduct(HttpSession session,Integer productId){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iCartService.selectOrUnSelect(currentUser.getId(),productId,Constants.Cart.CHECK);
    }

    @PutMapping("/cart_un_select")
    @ResponseBody
    public Result unSelectProduct(HttpSession session,Integer productId){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iCartService.selectOrUnSelect(currentUser.getId(),productId,Constants.Cart.UN_CHECK);
    }

    @PutMapping("/cart_update")
    @ResponseBody
    public Result updateCartProduct(HttpSession session,Integer newCount,Integer productId){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iCartService.updateCartProductCount(currentUser.getId(),newCount,productId);
    }

    @DeleteMapping("/delete_product")
    @ResponseBody
    public Result deleteCartProduct(HttpSession session,String productIds){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iCartService.deleteCartProductCount(currentUser.getId(),productIds);
    }


    @PostMapping("/cart_add")
    @ResponseBody
    public Result cart_add(@RequestBody Cart cart, HttpSession session){
        MallUserVO currentUser = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (currentUser == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        cart.setUserId(currentUser.getId());
        Result result = iCartService.cart_add(cart);

        int cart_count = iCartService.cart_count(cart.getUserId());
        currentUser.setCartItemCount(cart_count);
        session.setAttribute(Constants.MALL_USER_SESSION_KEY,currentUser);
        return result;
    }

}
