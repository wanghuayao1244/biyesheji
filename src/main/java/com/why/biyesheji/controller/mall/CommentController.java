package com.why.biyesheji.controller.mall;

import com.why.biyesheji.common.Constants;
import com.why.biyesheji.pojo.Comment;
import com.why.biyesheji.service.ICommentService;
import com.why.biyesheji.service.IProductService;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.vo.MallUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by why on 2021/1/31
 **/
@Controller
public class CommentController {

    @Autowired
    private IProductService iProductService;

    @Autowired
    private ICommentService iCommentService;


    @GetMapping({"replycomment","replycomment.html"})
    public String replyComment(@RequestParam("productId") Integer productId, Model model){
        Result productDetail = iProductService.productDetail(productId);
        model.addAttribute("productDetail",productDetail.getData());
        return "mall/productDetail-replycomments";
    }

    @PostMapping("/commentAdd")
    @ResponseBody
    public Result commentAdd(Comment comment,Integer productId, HttpSession session){
        MallUserVO mallUserVO = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        comment.setUsername(mallUserVO.getUsername());
        comment.setProductId(productId);
        Result result = iCommentService.commentAdd(comment);
        return result;
    }
}
