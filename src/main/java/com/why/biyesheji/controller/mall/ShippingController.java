package com.why.biyesheji.controller.mall;

import com.why.biyesheji.common.Constants;
import com.why.biyesheji.pojo.Shipping;
import com.why.biyesheji.service.IShippingService;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.util.ResultGenerator;
import com.why.biyesheji.vo.MallUserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * Created by 华耀 王 on 2021/1/4
 **/
@Controller
public class ShippingController {

    @Autowired
    private IShippingService iShippingService;

    // 添加地址
    @PostMapping("/address_add")
    @ResponseBody
    public Result addressAdd(@RequestBody Shipping shipping, HttpSession session){
        MallUserVO mallUserVO = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (mallUserVO == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        shipping.setUserId(mallUserVO.getId());
        Result result = iShippingService.addressAdd(shipping);
        return result;
    }

    // 回填地址
    @PostMapping("/address_selectById")
    @ResponseBody
    public Result addressSelectById(Integer shippingId, HttpSession session){
        MallUserVO mallUserVO = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (mallUserVO == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iShippingService.addressSelectById(mallUserVO.getId(), shippingId);
    }

    // 更新地址
    @PostMapping("/address_update")
    @ResponseBody
    public Result addressUpdate(@RequestBody Shipping shipping, HttpSession session){
        MallUserVO mallUserVO = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (mallUserVO == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        shipping.setUserId(mallUserVO.getId());
        return iShippingService.addressUpdate(shipping);
    }

    // 删除地址
    @PostMapping("/address_delete")
    @ResponseBody
    public Result addressDelete(Integer shippingId, HttpSession session){
        MallUserVO mallUserVO = (MallUserVO) session.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (mallUserVO == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        return iShippingService.addressDelete(mallUserVO.getId(),shippingId);
    }
}
