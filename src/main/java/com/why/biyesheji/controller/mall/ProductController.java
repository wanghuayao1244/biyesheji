package com.why.biyesheji.controller.mall;

import com.github.pagehelper.PageInfo;
import com.why.biyesheji.pojo.Product;
import com.why.biyesheji.service.ICommentService;
import com.why.biyesheji.service.IProductService;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.util.ResultGenerator;
import com.why.biyesheji.vo.ProductListVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 华耀 王 on 2020/11/2
 **/
@Controller
public class ProductController {

    @Autowired
    private IProductService iProductService;

    @Autowired
    private ICommentService iCommentService;

    @GetMapping({"/list","list.html"})
    public String list(@RequestParam(value = "keyword",required = false) String keyword,
                       @RequestParam(value = "categoryId",required = false) Integer categoryId,
                       @RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
                       @RequestParam(value = "pageSize",defaultValue = "5") int pageSize,
                       @RequestParam(value = "orderBy",defaultValue = "") String orderBy,
                       Model model
                        ){
        Result productList = iProductService.productList(keyword, categoryId, pageNum, pageSize, orderBy);
        PageInfo pageInfo = (PageInfo) productList.getData();
        List<ProductListVO> productListVOList = pageInfo.getList();
        model.addAttribute("keyword",keyword);
        model.addAttribute("orderBy",orderBy);
        model.addAttribute("categoryId",categoryId);
        model.addAttribute("pageInfo",pageInfo);
        model.addAttribute("productList",productListVOList);
        return "mall/list";
    }

    @GetMapping({"/detail","/detail.html"})
    public String productDetail(@RequestParam("productId") Integer productId,Model model){
        Result productDetail = iProductService.productDetail(productId);
        model.addAttribute("productDetail",productDetail.getData());
        return "mall/productDetail";
    }

    @GetMapping({"/detail_comment","/detail_comment.html"})
    public String productDetailComment(@RequestParam("productId") Integer productId,Model model){
        Result productDetail = iProductService.productDetail(productId);
        Result productComment = iCommentService.commentList(productId);
        model.addAttribute("productComment",productComment.getData());
        model.addAttribute("productDetail",productDetail.getData());
        return "mall/productDetail-comments";
    }









    @RequestMapping("/productAdd")
    @ResponseBody
    public Result productAdd(HttpSession session,@RequestBody Product product){
        int resultAdd = iProductService.productAdd(product);
        if (resultAdd > 0){
            return ResultGenerator.getSuccessResult("插入信息成功！");
        }
        return ResultGenerator.getFailResult("插入信息失败");
    }

    @RequestMapping("/productUpdate")
    @ResponseBody
    public Result productUpdate(HttpSession session,@RequestBody Product product){
        int resultAdd = iProductService.productUpdate(product);
        if (resultAdd > 0){
            return ResultGenerator.getSuccessResult("更新信息成功！");
        }
        return ResultGenerator.getFailResult("更新信息失败");
    }
}
