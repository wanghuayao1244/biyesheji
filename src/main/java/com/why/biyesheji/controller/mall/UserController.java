package com.why.biyesheji.controller.mall;

import com.why.biyesheji.common.Constants;
import com.why.biyesheji.common.ServiceResultEnum;
import com.why.biyesheji.dao.UserMapper;
import com.why.biyesheji.pojo.User;
import com.why.biyesheji.service.IUserService;
import com.why.biyesheji.util.MD5Util;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.util.ResultGenerator;
import com.why.biyesheji.vo.MallUserVO;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by 华耀 王 on 2020/10/13
 **/
@Controller
public class UserController {

    @Autowired
    private IUserService iUserService;

    /**
     * 用户模块
     * 登录跳转
     *
     * @param
     * @return
     */
    @GetMapping({"/login","login.html"})
    public String loginGet() {
        return "mall/login";
    }

    @GetMapping({"/register","register.html"})
    public String registerPage(){
        return "mall/register";
    }

    @GetMapping({"/result","result.html"})
    public String resultPage(){
        return "mall/result";
    }

    @GetMapping({"/user-pass-reset","user-pass-reset.html"})
    public String userPasswordReset(){
        return "mall/user-pass-reset";
    }

    @GetMapping({"/about","about.html"})
    public String about(){
        return "mall/about";
    }

    //退出登录
    @GetMapping({"/logout"})
    public String userLogout(HttpSession session){
        /*session.invalidate()表示将session中的变量全部清空。
        invalid是系统API函数，它的作用是刷新使整个窗口客户区无效，窗口的客户区无效意味着需要重绘，OnPaint负责重绘窗口。*/
        session.removeAttribute(Constants.MALL_USER_SESSION_KEY);
        return "mall/login";
    }

//    提交登录
    @PostMapping("/login")
    @ResponseBody
    public Result login(String username, String password, String verifyCode, HttpSession session, Model model){
        if (StringUtils.isEmpty(username)){
            return ResultGenerator.getFailResult(ServiceResultEnum.LOGIN_NAME_NULL.getResult());
        }
        if (StringUtils.isEmpty(password)){
            return ResultGenerator.getFailResult(ServiceResultEnum.LOGIN_PASSWORD_NULL.getResult());
        }
/*        if (StringUtils.isEmpty(verifyCode)){
            return ResultGenerator.getFailResult(ServiceResultEnum.LOGIN_VERIFY_CODE_NULL.getResult());
        }*/
        //验证码验证
/*        String kaptchaCode = session.getAttribute(Constants.MALL_VERIFY_CODE_KEY)+"";
        if (StringUtils.isEmpty(kaptchaCode) || !verifyCode.equals(kaptchaCode)){
            return ResultGenerator.getFailResult(ServiceResultEnum.LOGIN_VERIFY_CODE_ERROR.getResult());
        }*/
        String MD5Password = MD5Util.MD5EncodeUtf8(password);
        String loginResult = iUserService.login(username,MD5Password,session);

        //登录成功
        if (ServiceResultEnum.SUCCESS.getResult().equals(loginResult)) {
            return ResultGenerator.getSuccessResult();
        }
        //登录失败
        return ResultGenerator.getFailResult(loginResult);
    }

    @PostMapping("/check_username")
    @ResponseBody
    public Result check_username(String username){
        String validUsername = iUserService.check_username(username);
        if (ServiceResultEnum.REGISTER_NAME_EXIST.getResult().equals(validUsername)){
            return ResultGenerator.getFailResult("用户名已存在");
        }
        return ResultGenerator.getSuccessResult();
    }

    @PostMapping("/register")
    @ResponseBody
    public Result register(@RequestParam("username") String username,
                           @RequestParam("password") String password,
                           @RequestParam("phone") String phone,
                           @RequestParam("email") String email,
                           @RequestParam("question") String question,
                           @RequestParam("answer") String answer
    ){
        String resultRegister = iUserService.register(username, password, phone, email, question, answer);
        if (ServiceResultEnum.SUCCESS.getResult().equals(resultRegister)){
            return ResultGenerator.getSuccessResult();
        }
        return ResultGenerator.getFailResult("注册失败");
    }

    @PostMapping("/forget_get_question")
    @ResponseBody
    public Result forget_get_question(String username){
        String resultQuestion = iUserService.forget_get_question(username);
        if (ServiceResultEnum.LOGIN_NAME_NOTEXIST.getResult().equals(resultQuestion)){
            return ResultGenerator.getFailResult(resultQuestion);
        }
        return ResultGenerator.getSuccessResult(resultQuestion);
    }

    @PostMapping("/forget_check_answer")
    @ResponseBody
    public Result forget_check_answer(String username,String question,String answer){
        String resultQuestion = iUserService.forget_check_answer(username,question,answer);
        if ("false".equals(resultQuestion)){
            return ResultGenerator.getFailResult("问题的答案错误");
        }
        return ResultGenerator.getSuccessResult(resultQuestion);
    }

    @PostMapping("/forget_reset_password")
    @ResponseBody
    public Result forgetResetPassword(String username,String passwordNew,String forgetToken){
        String resultReset = iUserService.forgetResetPassword(username, passwordNew, forgetToken);
        if ("true".equals(resultReset)){
            return ResultGenerator.getSuccessResult(resultReset);
        }
        return ResultGenerator.getFailResult(resultReset);
    }

    //个人中心
    @GetMapping({"/user-center","user-center.html"})
    public String userCenterPage(){
        return "mall/user-center";
    }

    //去个人信息更新页面
    @GetMapping({"/user-center-update","user-center-update.html"})
    public String toUserUpdatePage(){
        return "mall/user-center-update";
    }

    //个人资料修改
    @PostMapping({"/user-center-update"})
    @ResponseBody
    public Result userCenterUpdatePage(@RequestBody User user,HttpSession httpSession){
        MallUserVO userSession = (MallUserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (userSession == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        String userResult = iUserService.updateUserInfo(user,httpSession);
        if (userResult == ServiceResultEnum.SUCCESS.getResult()){
            return ResultGenerator.getSuccessResult();
        }
        return ResultGenerator.getFailResult(userResult);
    }

    //去个人密码修改页面
    @GetMapping({"/user-pass-update","user-pass-update.html"})
    public String toUserPassUpdatePage(){
        return "mall/user-pass-update";
    }

    //个人密码修改
    @PostMapping({"/user-pass-update"})
    @ResponseBody
    public Result userPassUpdatePage(String passwordOld,String passwordNew,HttpSession httpSession){
        MallUserVO userSession = (MallUserVO) httpSession.getAttribute(Constants.MALL_USER_SESSION_KEY);
        if (userSession == null){
            return ResultGenerator.getFailResult(Constants.NEED_LOGIN);
        }
        String userResult = iUserService.updateUserPass(passwordOld,passwordNew,userSession);
        if (userResult == ServiceResultEnum.SUCCESS.getResult()){
            return ResultGenerator.getSuccessResult(userResult);
        }
        return ResultGenerator.getFailResult(userResult);
    }

}
