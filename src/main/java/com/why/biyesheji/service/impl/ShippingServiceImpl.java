package com.why.biyesheji.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.why.biyesheji.common.Constants;
import com.why.biyesheji.dao.ShippingMapper;
import com.why.biyesheji.pojo.Shipping;
import com.why.biyesheji.service.IShippingService;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.util.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by 华耀 王 on 2021/1/4
 **/
@Service
public class ShippingServiceImpl implements IShippingService {
    @Autowired
    private ShippingMapper shippingMapper;

    @Override
    public Result shippingList(Integer userId,Integer pageNum,Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Shipping> shippingList = shippingMapper.selectShippingListByUserId(userId);
        PageInfo pageInfo = new PageInfo(shippingList);
        return ResultGenerator.getSuccessResult(pageInfo);
    }

    @Override
    public Result addressAdd(Shipping shipping) {
        if (shipping == null){
            return ResultGenerator.getFailResult(Constants.ILLEGAL_ARGUMENT);
        }
        int insert = shippingMapper.insert(shipping);
        if (insert > 0){
            return ResultGenerator.getSuccessResult("插入地址成功");
        }
        return ResultGenerator.getFailResult("新建地址失败");
    }

    @Override
    public Result addressSelectById(Integer userId, Integer shippingId) {
        if (shippingId == null){
            return ResultGenerator.getFailResult(Constants.ILLEGAL_ARGUMENT);
        }
        Shipping shipping = shippingMapper.addressSelectById(userId,shippingId);
        if (shipping == null){
            return ResultGenerator.getFailResult("无法查询该地址");
        }
        return ResultGenerator.getSuccessResult("地址查询成功",shipping);
    }

    @Override
    public Result addressUpdate(Shipping shipping) {
        if (shipping == null){
            return ResultGenerator.getFailResult(Constants.ILLEGAL_ARGUMENT);
        }
        int update = shippingMapper.addressUpdate(shipping);
        if (update > 0){
            return ResultGenerator.getSuccessResult("地址更新成功");
        }
        return ResultGenerator.getFailResult("地址更新失败");
    }

    @Override
    public Result addressDelete(Integer userId, Integer shippingId) {
        if (shippingId == null){
            return ResultGenerator.getFailResult(Constants.ILLEGAL_ARGUMENT);
        }
        int addressDelete = shippingMapper.addressDelete(userId, shippingId);
        if (addressDelete > 0){
            return ResultGenerator.getSuccessResult("删除地址成功！");
        }
        return ResultGenerator.getFailResult("删除地址失败！");
    }

}
