package com.why.biyesheji.service.impl;

import com.google.common.collect.Lists;
import com.why.biyesheji.dao.CommentMapper;
import com.why.biyesheji.pojo.Comment;
import com.why.biyesheji.service.ICommentService;
import com.why.biyesheji.util.DateTimeUtil;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.util.ResultGenerator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by why on 2021/1/31
 **/
@Service("iCommentService")
public class CommentServiceImpl implements ICommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Override
    public Result commentList(Integer productId) {
        List<Comment> commentList = commentMapper.commentList(productId);
        return ResultGenerator.getSuccessResult(commentList);
    }

    @Override
    public Result commentAdd(Comment comment) {
        if (comment != null){
            int insert = commentMapper.insert(comment);
            if (insert > 0){
                return ResultGenerator.getSuccessResult("评论成功！");
            }
        }
        return ResultGenerator.getSuccessResult("参数有误，插入数据错误");
    }
}
