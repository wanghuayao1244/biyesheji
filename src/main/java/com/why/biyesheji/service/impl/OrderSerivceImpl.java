package com.why.biyesheji.service.impl;

import com.alipay.api.AlipayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.model.ExtendParams;
import com.alipay.demo.trade.model.GoodsDetail;
import com.alipay.demo.trade.model.builder.AlipayTradePrecreateRequestBuilder;
import com.alipay.demo.trade.model.result.AlipayF2FPrecreateResult;
import com.alipay.demo.trade.service.AlipayTradeService;
import com.alipay.demo.trade.service.impl.AlipayTradeServiceImpl;
import com.alipay.demo.trade.utils.ZxingUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.why.biyesheji.common.Constants;
import com.why.biyesheji.dao.*;
import com.why.biyesheji.pojo.*;
import com.why.biyesheji.service.IOrderService;
import com.why.biyesheji.util.*;
import com.why.biyesheji.vo.OrderItemVo;
import com.why.biyesheji.vo.OrderProductVo;
import com.why.biyesheji.vo.OrderVo;
import com.why.biyesheji.vo.ShippingVO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 获取购物车里的商品项
 * 创建订单
 * 取消订单
 * Created by why on 2021/3/1
 **/
@Service
public class OrderSerivceImpl implements IOrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderSerivceImpl.class);

    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ShippingMapper shippingMapper;

    @Autowired
    private PayInfoMapper payInfoMapper;

    /**
     * 获取订单确认页的商品列表
     * @param userId
     * @return
     */
    @Override
    public Result getProductList(Integer userId) {

        OrderProductVo orderProductVo = new OrderProductVo();

        List<Cart> cartList = cartMapper.selectListByUserId(userId);
        Result result = this.getCartOrderItem(userId,cartList);
        if (result.getResultCode() != 200){
            return result;
        }

        List<OrderItem> orderItemList = (List<OrderItem>) result.getData();

        List<OrderItemVo> orderItemVoList = Lists.newArrayList();

        BigDecimal payment = new BigDecimal("0");
        for (OrderItem orderItem : orderItemList) {
            payment = BigDecimalUtil.add(payment.doubleValue(),orderItem.getTotalPrice().doubleValue());
            orderItemVoList.add(assembleOrderItemVo(orderItem));
        }

        orderProductVo.setOrderItemVoList(orderItemVoList);
        orderProductVo.setProductTotalPrice(payment);
        orderProductVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));

        return ResultGenerator.getSuccessResult(orderProductVo);
    }

    /**
     * 创建订单
     * @param userId
     * @param shippingId
     * @return
     */
    @Override
    public Result createOrder(Integer userId, Integer shippingId) {
        // 购物车获取商品
        List<Cart> cartList = cartMapper.selectListByUserId(userId);

        Result orderItemResult = this.getCartOrderItem(userId,cartList);
        if (orderItemResult.getResultCode() != 200){
            return orderItemResult;
        }

        List<OrderItem> orderItemList = (List<OrderItem>) orderItemResult.getData();
        // 计算订单总价
        BigDecimal payment = this.getOrderTotalPrice(orderItemList);
        // 生成订单
        Order order = this.assembleOrder(userId,shippingId,payment);
        if (order == null){
            return ResultGenerator.getFailResult("生成订单错误");
        }
        if (CollectionUtils.isEmpty(orderItemList)){
            return ResultGenerator.getFailResult("购物车为空");
        }
        // 给orderItem填上orderNo
        for (OrderItem orderItem : orderItemList) {
            orderItem.setOrderNo(order.getOrderNo());
        }

        // 批量插入orderItem
        orderItemMapper.batchInsert(orderItemList);

        // 生成订单成功，我们要减少我们产品的库存，
        this.reduceProductStock(orderItemList);
        
        // 清空购物车
        this.clearCart(cartList);
        
        //返回前端数据，创建一个Vo类将各种数据返回给前端
        OrderVo orderVo = assembleOrderVo(order,orderItemList);
        return ResultGenerator.getSuccessResult(orderVo);
    }



    @Override
    public Result pay(Integer userId, Long orderNo, String upload) {
        Map<String,String> resultMap = Maps.newHashMap();// 作为一个结果集返回
        Order order = orderMapper.selectByUserIdOrderNo(userId,orderNo);
        if (order == null){
            return ResultGenerator.getFailResult("找不到该用户订单信息");
        }
        resultMap.put("orderNo", String.valueOf(order.getOrderNo()));

        // (必填) 商户网站订单系统中唯一订单号，64个字符以内，只能包含字母、数字、下划线，
        // 需保证商户系统端不能重复，建议通过数据库sequence生成，
        String outTradeNo = order.getOrderNo().toString();

        // (必填) 订单标题，粗略描述用户的支付目的。如“xxx品牌xxx门店当面付扫码消费”
        String subject = new StringBuilder().append("magic_shop扫码支付，订单号：").append(outTradeNo).toString();

        // (必填) 订单总金额，单位为元，不能超过1亿元
        // 如果同时传入了【打折金额】,【不可打折金额】,【订单总金额】三者,则必须满足如下条件:【订单总金额】=【打折金额】+【不可打折金额】
        String totalAmount = order.getPayment().toString();

        // (可选) 订单不可打折金额，可以配合商家平台配置折扣活动，如果酒水不参与打折，则将对应金额填写至此字段
        // 如果该值未传入,但传入了【订单总金额】,【打折金额】,则该值默认为【订单总金额】-【打折金额】
        String undiscountableAmount = "0";

        // 卖家支付宝账号ID，用于支持一个签约账号下支持打款到不同的收款账号，(打款到sellerId对应的支付宝账号)
        // 如果该字段为空，则默认为与支付宝签约的商户的PID，也就是appid对应的PID
        String sellerId = "";

        // 订单描述，可以对交易或商品进行一个详细地描述，比如填写"购买商品2件共15.00元"
        String body = new StringBuilder().append("订单").append(order.getOrderNo()).append("购买商品共").append(order.getPayment()).append("元").toString();

        // 商户操作员编号，添加此参数可以为商户操作员做销售统计
        String operatorId = "test_operator_id";

        // (必填) 商户门店编号，通过门店号和商家后台可以配置精准到门店的折扣信息，详询支付宝技术支持
        String storeId = "test_store_id";

        // 业务扩展参数，目前可添加由支付宝分配的系统商编号(通过setSysServiceProviderId方法)，详情请咨询支付宝技术支持
        ExtendParams extendParams = new ExtendParams();
        extendParams.setSysServiceProviderId("2088100200300400500");

        // 支付超时，定义为120分钟
        String timeoutExpress = "120m";

        // 商品明细列表，需填写购买商品详细信息，
        List<GoodsDetail> goodsDetailList = new ArrayList<GoodsDetail>();

        // 创建一个商品信息，参数含义分别为商品id（使用国标）、名称、单价（单位为分）、数量，如果需要添加商品类别，详见GoodsDetail
        List<OrderItem> orderItemList = orderItemMapper.getByOrderNoUserId(orderNo,userId);
        for (OrderItem orderItem : orderItemList) {
            GoodsDetail goods = GoodsDetail.newInstance(orderItem.getProductId().toString(), orderItem.getProductName(),
                    BigDecimalUtil.mul(orderItem.getCurrentUnitPrice().doubleValue(),new Double(100).doubleValue()).longValue(), orderItem.getQuantity());
            // 创建好一个商品后添加至商品明细列表
            goodsDetailList.add(goods);
        }

        // 创建扫码支付请求builder，设置请求参数
        AlipayTradePrecreateRequestBuilder builder = new AlipayTradePrecreateRequestBuilder()
                .setSubject(subject).setTotalAmount(totalAmount).setOutTradeNo(outTradeNo)
                .setUndiscountableAmount(undiscountableAmount).setSellerId(sellerId).setBody(body)
                .setOperatorId(operatorId).setStoreId(storeId).setExtendParams(extendParams)
                .setTimeoutExpress(timeoutExpress)
                .setNotifyUrl(PropertiesUtil.getProperty("alipay.callback.url"))//支付宝服务器主动通知商户服务器里指定的页面http路径,根据需要设置
                .setGoodsDetailList(goodsDetailList);

        /** 一定要在创建AlipayTradeService之前调用Configs.init()设置默认参数
         *  Configs会读取classpath下的zfbinfo.properties文件配置信息，如果找不到该文件则确认该文件是否在classpath目录
         */
        Configs.init("zfbinfo.properties");

        /** 使用Configs提供的默认参数
         *  AlipayTradeService可以使用单例或者为静态成员对象，不需要反复new
         */
        // 支付宝当面付2.0服务
        AlipayTradeService tradeService = new AlipayTradeServiceImpl.ClientBuilder().build();

        AlipayF2FPrecreateResult result = tradeService.tradePrecreate(builder);
        switch (result.getTradeStatus()) {
            case SUCCESS:
                logger.info("支付宝预下单成功: )");

                AlipayTradePrecreateResponse response = result.getResponse();
                dumpResponse(response);

                // 生成二维码，返回给服务器，服务器再返回给前端
                File folder = new File(upload);
                if (!folder.exists()){
                    folder.setWritable(true);
                    folder.mkdirs();
                }

                // 需要修改为运行机器上的路径
                //注意注意，别漏了“/”
                // 需要修改为运行机器上的路径
                String qrPath = String.format(upload+"/qr-%s.png", response.getOutTradeNo());
                String qrFileName = String.format("qr-%s.png", response.getOutTradeNo());
                ZxingUtils.getQRCodeImge(response.getQrCode(), 256, qrPath);

                File targetFile = new File(upload,qrFileName);
                try {
                    FTPUtil.uploadFile(Lists.newArrayList(targetFile));
                } catch (IOException e) {
                    logger.info("二维码上传FTP服务器异常"+e);
                }
                //               生成二维码： ZxingUtils.getQRCodeImge(response.getQrCode(), 256, filePath);
                logger.info("qrPath:"+qrPath);
                String qrUrl =PropertiesUtil.getProperty("ftp.server.http.prefix")+targetFile.getName();
                logger.info("qrUrl:"+qrUrl);
                resultMap.put("qrUrl",qrUrl);
                return ResultGenerator.getSuccessResult(resultMap);
            case FAILED:
                logger.error("支付宝预下单失败!!!");
                return ResultGenerator.getFailResult("支付宝预下单失败!!!");
            case UNKNOWN:
                logger.error("系统异常，预下单状态未知!!!");
                return ResultGenerator.getFailResult("系统异常，预下单状态未知!!!");

            default:
                logger.error("不支持的交易状态，交易返回异常!!!");
                return ResultGenerator.getFailResult("不支持的交易状态，交易返回异常!!!");
        }
    }

    /**
     * 支付宝返回的信息：
     * 商户的订单号 ： out_trade_no
     * 支付宝交易流水号 ：trade_no
     * 交易状态 ： trade_status
     */
    @Override
    public Result alipayCallBack(Map<String, String> params) {
        Long orderNo = Long.parseLong(params.get("out_trade_no"));
        String total_amount = params.get("total_amount");
        String tradeNo = params.get("trade_no");
        String traderStatus = params.get("trade_status");
        Order order = orderMapper.selectByOrderNo(orderNo);
        if (order == null){
            return ResultGenerator.getFailResult("非magic_shop的订单，忽略回调");
        }
        if ((new BigDecimal(total_amount).compareTo(order.getPayment())) != 0){
            return ResultGenerator.getFailResult("订单金额错误，忽略回调");
        }
        if (order.getStatus() >= Constants.OrderStatusEnum.PAID.getCode()){
            return ResultGenerator.getFailResult("支付宝重复调用");
        }
        if (Constants.AlipayCallBack.TRADE_STATUS_TRADE_SUCCESS.equals(traderStatus)){
            order.setPaymentTime(DateTimeUtil.strToDate(params.get("gmt_payment")));
            order.setStatus(Constants.OrderStatusEnum.PAID.getCode());
            orderMapper.updateByPrimaryKeySelective(order);
        }

        PayInfo payInfo = new PayInfo();
        payInfo.setUserId(order.getUserId());
        payInfo.setOrderNo(order.getOrderNo());
        payInfo.setPayPlaftform(Constants.PayPlatformEnum.ALIPAY.getCode());
        payInfo.setPlaftformNumber(tradeNo);
        payInfo.setPlaftformStatus(traderStatus);

        payInfoMapper.insert(payInfo);

        return ResultGenerator.getSuccessResult();
    }

    @Override
    public Result queryOrderPayStatus(Integer userId, Long orderNo) {
        Order order = orderMapper.selectByUserIdOrderNo(userId, orderNo);
        if (order == null){
            return ResultGenerator.getFailResult("该用户没有订单");
        }
        if (order.getStatus()>= Constants.OrderStatusEnum.PAID.getCode()){
            return ResultGenerator.getSuccessResult();
        }
        return ResultGenerator.getFailResult();
    }

    /**
     * 查询订单列表在组装
     * @param userId
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public Result getOrderList(Integer userId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderList = orderMapper.selectByUserId(userId);
        List<OrderVo> orderVoList = assembleOrderVoList(orderList,userId);
        PageInfo pageInfo = new PageInfo(orderList); // 传入原来查询出来的数据进行分页
        pageInfo.setList(orderVoList);// 前台显示的数据
        return ResultGenerator.getSuccessResult(pageInfo);
    }

    @Override
    public Result getOrderDetail(Integer userId, Long orderNumber) {
        Order order = orderMapper.selectByUserIdOrderNo(userId, orderNumber);
        if (order != null){
            List<OrderItem> orderItemList = orderItemMapper.selectByOrderNo(orderNumber);
            OrderVo orderVo = assembleOrderVo(order,orderItemList);
            return ResultGenerator.getSuccessResult(orderVo);
        }
        return ResultGenerator.getFailResult("没有找到该订单！");
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result orderCancel(Integer userId, Long orderNo) {
        Order order = orderMapper.selectByUserIdOrderNo(userId, orderNo);
        if (order == null){
            return ResultGenerator.getFailResult("订单取消失败，订单不存在！");
        }
        if (order.getStatus() != Constants.OrderStatusEnum.ON_PAY.getCode()){
            return ResultGenerator.getFailResult("订单已付款，无法取消订单！");
        }

        Order updateOrder = new Order();
        updateOrder.setId(order.getId());
        updateOrder.setStatus(Constants.OrderStatusEnum.CANCELED.getCode());
        int update = orderMapper.updateByPrimaryKeySelective(updateOrder);
        if (update > 0){
            return ResultGenerator.getSuccessResult();
        }
        return ResultGenerator.getFailResult();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result orderReceive(Integer userId, Long orderNo) {
        Order order = orderMapper.selectByUserIdOrderNo(userId, orderNo);
        if (order == null){
            return ResultGenerator.getFailResult("订单取消失败，订单不存在！");
        }
        Order updateOrder = new Order();
        updateOrder.setId(order.getId());
        updateOrder.setStatus(Constants.OrderStatusEnum.ORDER_SUCCESS.getCode());
        int update = orderMapper.updateByPrimaryKeySelective(updateOrder);
        if (update > 0){
            return ResultGenerator.getSuccessResult();
        }
        return ResultGenerator.getFailResult();
    }

    private List<OrderVo> assembleOrderVoList(List<Order> orderList, Integer userId) {
        List<OrderVo> orderVoList = Lists.newArrayList();
        for (Order order : orderList) {
            List<OrderItem> orderItemList = Lists.newArrayList();
            if (userId == null){
                orderItemList = orderItemMapper.selectByOrderNo(order.getOrderNo());
            }else {
                orderItemList = orderItemMapper.getByOrderNoUserId(order.getOrderNo(),userId);
            }
            OrderVo orderVo = assembleOrderVo(order,orderItemList);
            orderVoList.add(orderVo);
        }
        return orderVoList;
    }

    // 简单打印应答
    private void dumpResponse(AlipayResponse response) {
        if (response != null) {
            logger.info(String.format("code:%s, msg:%s", response.getCode(), response.getMsg()));
            if (StringUtils.isNotEmpty(response.getSubCode())) {
                logger.info(String.format("subCode:%s, subMsg:%s", response.getSubCode(),
                        response.getSubMsg()));
            }
            logger.info("body:" + response.getBody());
        }
    }



    // 组装OrderVo
    private OrderVo assembleOrderVo(Order order, List<OrderItem> orderItemList) {
        OrderVo orderVo = new OrderVo();
        orderVo.setOrderNo(order.getOrderNo());
        orderVo.setPayment(order.getPayment());
        orderVo.setPaymentType(order.getPaymentType());
        orderVo.setPaymentTypeDesc(Constants.PaymentTypeEnum.codeof(order.getPaymentType()).getValue());

        orderVo.setPostage(order.getPostage());
        orderVo.setStatus(order.getStatus());
        orderVo.setStatusDesc(Constants.OrderStatusEnum.codeof(order.getStatus()).getValue());

        orderVo.setPaymentTime(DateTimeUtil.dateToStr(order.getPaymentTime()));
        orderVo.setSendTime(DateTimeUtil.dateToStr(order.getSendTime()));
        orderVo.setEndTime(DateTimeUtil.dateToStr(order.getEndTime()));
        orderVo.setCloseTime(DateTimeUtil.dateToStr(order.getCloseTime()));
        orderVo.setCreateTime(DateTimeUtil.dateToStr(order.getCreateTime()));

        List<OrderItemVo> orderItemVoList = Lists.newArrayList();
        for (OrderItem orderItem : orderItemList) {
            OrderItemVo orderItemVo = assembleOrderItemVo(orderItem);
            orderItemVoList.add(orderItemVo);
        }
        orderVo.setOrderItemVoList(orderItemVoList);

        orderVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        orderVo.setShippingId(order.getShippingId());
        Shipping shipping = shippingMapper.selectByPrimaryKey(order.getShippingId());
        if (shipping != null){
            orderVo.setReceiverName(shipping.getReceiverName());
            orderVo.setShippingVO(assembleShippingVo(shipping));
        }

        return orderVo;
    }

    // 组装ShippingVo
    private ShippingVO assembleShippingVo(Shipping shipping) {
        ShippingVO shippingVO = new ShippingVO();
        shippingVO.setReceiverName(shipping.getReceiverName());
        shippingVO.setReceiverPhone(shipping.getReceiverPhone());
        shippingVO.setReceiverProvince(shipping.getReceiverProvince());
        shippingVO.setReceiverCity(shipping.getReceiverCity());
        shippingVO.setReceiverAddress(shipping.getReceiverAddress());
        shippingVO.setReceiverZip(shipping.getReceiverZip());
        return shippingVO;
    }

    // 清空购物车
    private void clearCart(List<Cart> cartList) {
        for (Cart cartItem : cartList) {
            cartMapper.deleteByPrimaryKey(cartItem.getId());
        }
    }

    // 减少产品库存
    private void reduceProductStock(List<OrderItem> orderItemList) {
        for (OrderItem orderItem : orderItemList) {
            Product product = productMapper.selectByPrimaryKey(orderItem.getProductId());
            product.setStock(product.getStock() - orderItem.getQuantity());
            productMapper.updateByPrimaryKeySelective(product);
        }
    }

    // 组装订单并插入数据库
    private Order assembleOrder(Integer userId, Integer shippingId, BigDecimal payment) {
        Order order = new Order();
        // 生成订单号
        long orderNo = this.generateOrderNo();
        order.setOrderNo(orderNo);
        order.setUserId(userId);
        order.setShippingId(shippingId);
        order.setPayment(payment);
        order.setPaymentType(Constants.PaymentTypeEnum.ONLINE_PAY.getCode());
        order.setPostage(0);
        order.setStatus(Constants.OrderStatusEnum.ON_PAY.getCode());
        // 发货和付款时间在执行对应操作时在填上；

        int insert = orderMapper.insert(order);
        if (insert > 0){
            return order;
        }
        return null;

    }

    // 生成订单号，现在的时间戳加上一个一百以内的随机数
    private long generateOrderNo() {
        long currentTime = System.currentTimeMillis();
        return currentTime+new Random().nextInt(100);
    }

    // 计算订单总价
    private BigDecimal getOrderTotalPrice(List<OrderItem> orderItemList) {
        BigDecimal payment = new BigDecimal("0");
        for (OrderItem orderItem : orderItemList) {
            payment = BigDecimalUtil.add(payment.doubleValue(),orderItem.getTotalPrice().doubleValue());
        }
        return payment;
    }

    // 封装orderItemVo
    private OrderItemVo assembleOrderItemVo(OrderItem orderItem) {
        OrderItemVo orderItemVo = new OrderItemVo();
        orderItemVo.setOrderNo(orderItem.getOrderNo());
        orderItemVo.setProductId(orderItem.getProductId());
        orderItemVo.setProductName(orderItem.getProductName());
        orderItemVo.setProductImage(orderItem.getProductImage());
        orderItemVo.setCurrentUnitPrice(orderItem.getCurrentUnitPrice());
        orderItemVo.setQuantity(orderItem.getQuantity());
        orderItemVo.setTotalPrice(orderItem.getTotalPrice());
        orderItemVo.setCreateTime(DateTimeUtil.dateToStr(orderItem.getCreateTime()));
        return orderItemVo;
    }

    // 获取购物车的订单项,商品单项的信息，单项商品的总价
    private Result getCartOrderItem(Integer userId, List<Cart> cartList) {
        List<OrderItem> orderItemList = Lists.newArrayList();
        if (CollectionUtils.isEmpty(cartList)){
            return ResultGenerator.getFailResult("购物车为空");
        }

        // 校验购物车里面的产品在线状态和库存
        for (Cart cartItem : cartList) {
            OrderItem orderItem = new OrderItem();
            Product product = productMapper.selectByPrimaryKey(cartItem.getProductId());
            if (product.getStatus() != Constants.ProductStatusEnum.ON_SALE.getCode()){
                return ResultGenerator.getFailResult("产品"+product.getName()+"不是在线状态");
            }

            // 校验库存
            if (cartItem.getQuantity() > product.getStock()){
                return ResultGenerator.getFailResult("产品"+product.getName()+"库存不足");
            }

            orderItem.setUserId(userId);
            orderItem.setProductId(product.getId());
            orderItem.setProductName(product.getName());
            orderItem.setProductImage(product.getMainImage());
            orderItem.setCurrentUnitPrice(product.getPrice());
            orderItem.setQuantity(cartItem.getQuantity());
            orderItem.setTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(),cartItem.getQuantity()));
            orderItemList.add(orderItem);
        }
        return ResultGenerator.getSuccessResult(orderItemList);
    }





}
