package com.why.biyesheji.service;

import com.why.biyesheji.util.Result;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * Created by 华耀 王 on 2020/3/23
 **/

public interface IFileService {
    Result uploadFile(MultipartFile file) throws Exception;
}
