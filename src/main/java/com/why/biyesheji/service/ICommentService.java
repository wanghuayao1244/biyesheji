package com.why.biyesheji.service;

import com.why.biyesheji.pojo.Comment;
import com.why.biyesheji.util.Result;

/**
 * Created by why on 2021/1/31
 **/

public interface ICommentService {
    Result commentList(Integer productId);

    Result commentAdd(Comment comment);
}
