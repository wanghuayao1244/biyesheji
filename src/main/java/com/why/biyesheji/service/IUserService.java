package com.why.biyesheji.service;

import com.why.biyesheji.pojo.User;
import com.why.biyesheji.vo.MallUserVO;

import javax.servlet.http.HttpSession;

/**
 * Created by 华耀 王 on 2020/10/13
 **/

public interface IUserService {
    String login(String username,String password, HttpSession httpSession);
    String check_username(String username);
    String register(String username,String password,String phone,String email,
             String question,String answer);

    String forget_get_question(String username);

    String forget_check_answer(String username,String question,String answer);

    String forgetResetPassword(String username,String passwordNew,String forgetToken);

    String updateUserInfo(User user,HttpSession httpSession);

    String updateUserPass(String passwordOld, String passwordNew, MallUserVO mallUserVO);

}
