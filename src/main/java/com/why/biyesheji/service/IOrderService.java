package com.why.biyesheji.service;

import com.why.biyesheji.util.Result;

import java.util.Map;

/**
 * Created by why on 2021/3/1
 **/

public interface IOrderService {
    Result getProductList(Integer userId);

    Result createOrder(Integer userId, Integer shippingId);

    Result pay(Integer userId, Long orderNo, String upload);

    Result alipayCallBack(Map<String, String> params);

    Result queryOrderPayStatus(Integer userId, Long orderNo);

    Result getOrderList(Integer userId, int pageNum, int pageSize);

    Result getOrderDetail(Integer userId, Long orderNumber);

    Result orderCancel(Integer userId, Long orderNo);

    Result orderReceive(Integer userId, Long orderNo);
}
