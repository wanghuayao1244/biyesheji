package com.why.biyesheji.service;

import com.why.biyesheji.pojo.Product;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.vo.IndexNewProductVO;

import java.util.List;

/**
 * Created by 华耀 王 on 2020/11/12
 **/

public interface IProductService {

    int productAdd(Product product);
    int productUpdate(Product product);
    List<IndexNewProductVO> newProduct();

    Result productList(String keyword, Integer categoryId, int pageNum, int pageSize, String orderBy);

    Result productDetail(Integer id);
}
