package com.why.biyesheji.service;

import com.why.biyesheji.pojo.Cart;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.vo.CartListVO;

/**
 * Created by 华耀 王 on 2020/12/22
 **/

public interface ICartService {
    Result cart_add(Cart cart);
    int cart_count(Integer userId);
    CartListVO cartList(Integer userId);
    boolean selectAllChecked(Integer userId);

    Result selectOrUnSelect(Integer userId, Integer productId, Integer check);

    Result updateCartProductCount(Integer userId, Integer newCount, Integer productId);

    Result deleteCartProductCount(Integer userId, String productIds);
}
