package com.why.biyesheji.service;

import java.util.List;

/**
 * Created by 华耀 王 on 2020/12/20
 **/

public interface ICategoryService {
    List<Integer> selectCategoryAndChildById(Integer categoryId);
}
