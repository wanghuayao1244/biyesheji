package com.why.biyesheji.service;

import com.why.biyesheji.pojo.Shipping;
import com.why.biyesheji.util.Result;

/**
 * Created by 华耀 王 on 2021/1/4
 **/

public interface IShippingService {
    Result shippingList(Integer userId,Integer pageNum,Integer pageSize);

    Result addressAdd(Shipping shipping);

    Result addressSelectById(Integer userId,Integer shippingId);

    Result addressUpdate(Shipping shipping);

    Result addressDelete(Integer userId, Integer shippingId);
}
