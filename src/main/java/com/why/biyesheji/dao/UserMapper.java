package com.why.biyesheji.dao;

import com.why.biyesheji.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> selectAll();


    User login(@Param("username")String username,@Param("password")String password);

    int checkUsername(String username);

    String selectQuestion(String username);

    int checkAnswer(@Param("username")String username,@Param("question")String question,@Param("answer")String answer);

    int updatePasswordByUsername(@Param("username")String username, @Param("passwordNew")String passwordNew);

    int checkPassword(@Param("passwordOld")String passwordOld, @Param("userId")Integer userId);

    int updatePassword(@Param("passwordNew")String passwordNew,@Param("userId")Integer userId);
}