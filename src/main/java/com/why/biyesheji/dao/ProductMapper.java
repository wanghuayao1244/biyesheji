package com.why.biyesheji.dao;

import com.why.biyesheji.pojo.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ProductMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    int productAdd(@Param("mainImage") String mainImage,@Param("subImages") String subImages,@Param("detail") String detail);

    List<Product> newProduct();

    List<Product> selectByNameAndCategoryIds(@Param("keyword") String keyword,@Param("categoryList")List<Integer> categoryIdList);


}