package com.why.biyesheji.dao;

import com.why.biyesheji.pojo.Cart;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Cart record);

    int insertSelective(Cart record);

    Cart selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);

    Cart selectProductExist(@Param("userId")Integer userId,@Param("productId") Integer productId);

    Integer selectCartCountByUserId(Integer userId);

    List<Cart> selectListByUserId(Integer userId);

    int selectProductAllCheckedByuserId(Integer userId);

    int selectOrUnSelect(@Param("userId")Integer userId, @Param("productId")Integer productId, @Param("check")Integer check);

    int updateQuantityByUserIdProductId(@Param("userId")Integer userId, @Param("newCount")Integer newCount, @Param("productId")Integer productId);

    int deleteCartByUserIdProductId(@Param("userId")Integer userId, @Param("productIds")List<String> productIds);
}