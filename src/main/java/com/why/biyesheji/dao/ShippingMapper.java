package com.why.biyesheji.dao;

import com.why.biyesheji.pojo.Shipping;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ShippingMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Shipping record);

    int insertSelective(Shipping record);

    Shipping selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Shipping record);

    int updateByPrimaryKey(Shipping record);

    List<Shipping> selectShippingListByUserId(Integer userId);

    Shipping addressSelectById(@Param("userId") Integer userId,@Param("shippingId") Integer shippingId);

    int addressUpdate(Shipping shipping);

    int addressDelete(@Param("userId")Integer userId,@Param("shippingId") Integer shippingId);
}