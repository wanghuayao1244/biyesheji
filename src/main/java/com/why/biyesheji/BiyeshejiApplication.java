package com.why.biyesheji;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan(basePackages = "com.why.biyesheji.dao")
@EnableTransactionManagement
public class BiyeshejiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BiyeshejiApplication.class, args);
    }

}
