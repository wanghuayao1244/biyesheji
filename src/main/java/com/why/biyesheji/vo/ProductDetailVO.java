package com.why.biyesheji.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 华耀 王 on 2020/12/22
 **/
@Data
public class ProductDetailVO {
    private Integer id;

    private Integer category;

    private String name;

    private String subtitle;

    private String mainImage;

    private List<String> subImages;

    private String detail;

    private BigDecimal price;

    private Integer stock;

    private Integer status;

    private Date createTime;

    private Date updateTime;

    private String imageHost;

    private Integer parentCategoryId;
}
