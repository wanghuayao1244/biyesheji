package com.why.biyesheji.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 订单确认页 Vo
 * params 订单项、订单总价、图片Host
 * Created by why on 2021/3/1
 **/
@Data
public class OrderProductVo {

    private List<OrderItemVo> orderItemVoList;
    private BigDecimal productTotalPrice;
    private String imageHost;
}
