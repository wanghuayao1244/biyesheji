package com.why.biyesheji.vo;

import lombok.Data;

import java.util.Date;

/**
 * Created by 华耀 王 on 2021/1/4
 **/
@Data
public class ShippingVO {

    private String receiverName;

    private String receiverPhone;

    private String receiverProvince;

    private String receiverCity;

    private String receiverAddress;

    private String receiverZip;

}
