package com.why.biyesheji.vo;

/**
 * Created by 华耀 王 on 2020/11/12
 **/

public class WangEditorVO {

    private Integer errno;
    private String[] data;

    public WangEditorVO() {
    }

    public WangEditorVO(Integer errno, String[] data) {
        this.errno = errno;
        this.data = data;
    }

    public Integer getErrno() {
        return errno;
    }

    public void setErrno(Integer errno) {
        this.errno = errno;
    }

    public String[] getData() {
        return data;
    }

    public void setData(String[] data) {
        this.data = data;
    }
}