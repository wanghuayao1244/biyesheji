package com.why.biyesheji.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by 华耀 王 on 2020/10/25
 **/
@Data
public class MallUserVO implements Serializable {
    private Integer id;
    private String username;
    private String email;
    private String phone;
    private String question;
    private String answer;
    private Integer CartItemCount;
}
