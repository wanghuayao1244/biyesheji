package com.why.biyesheji.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by 华耀 王 on 2020/12/31
 **/
@Data
public class CartProductVO {

    private Integer id;

    private Integer userId;

    private Integer productId;

    private String productName;

    private Integer quantity;

    private Integer productChecked;

    private String productMainImage;

    private BigDecimal productPrice;

    private BigDecimal productTotalPrice;

    private Integer productStock;

}
