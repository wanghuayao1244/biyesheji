package com.why.biyesheji.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by 华耀 王 on 2020/11/5
 **/
@Data
public class ProductListVO {
    private Integer id;
    private Integer categoryId;
    private String name;
    private String subtitle;
    private BigDecimal price;
    private String mainImage;
    private Integer status;

    private String imageHost;
}
