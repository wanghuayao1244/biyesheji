package com.why.biyesheji.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by 华耀 王 on 2020/12/31
 **/
@Data
public class CartListVO {


    private List<CartProductVO> cartProductVOList;

    private BigDecimal cartTotalPrice;

    private BigDecimal productCountTotalPrice;

    private boolean allChecked;

    private String imageHost;

}
