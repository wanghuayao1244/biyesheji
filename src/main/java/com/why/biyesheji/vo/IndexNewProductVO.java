package com.why.biyesheji.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by 华耀 王 on 2020/12/19
 **/
@Data
public class IndexNewProductVO {
    private String imageHost;
    private String mainImage;
    private Integer id;
    private String name;
    private String subtitle;
    private BigDecimal price;
}
