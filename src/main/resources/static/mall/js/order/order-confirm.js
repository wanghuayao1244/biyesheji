
// 模态框的关闭和显示，地址的添加
$(document).on('click','.address-add',function () {
    $('#modal-address').modal('show');
})
$(document).on('click','#close-modal-address',function () {
    $('#modal-address').modal('hide');
})
// 模态框的关闭和显示，地址的修改
$(document).on('click','.address-update',function () {
    $('#modal-address-edit').modal('show');
    var addressId = $(this).parents('.address-item').data('id');
    $.ajax({
        type: 'POST',
        url: '/address_selectById',
        data: { shippingId : addressId},
        success : function (result) {
            var id = result.data.id;
            $('#receiver-id-edit').val(id);

            var receiverName = result.data.receiverName;
            $('#receiver-name-edit').val(receiverName);

            var receiverProvince = result.data.receiverProvince;
            $('#receiver-province-edit').val(receiverProvince);

            var receiverCity = result.data.receiverCity;
            $('#receiver-city-edit').val(receiverCity);

            var receiverAddress = result.data.receiverAddress;
            $('#receiver-address-edit').val(receiverAddress);

            var receiverPhone = result.data.receiverPhone;
            $('#receiver-phone-edit').val(receiverPhone);

            var receiverZip = result.data.receiverZip;
            $('#receiver-zip-edit').val(receiverZip);
        }
    })
})
$(document).on('click','#close-modal-address-edit',function () {
    $('#modal-address-edit').modal('hide');
})

// 提交收货地址
$(document).on('click','.address-btn',function () {
    // 验证字段
    var receiverName = $('#receiver-name').val();
    if (isNull(receiverName)){
        swal('收货人名称不能为空',{
            icon: "error"
        })
        return false;
    }

    var receiverProvince = $('#receiver-province').val();
    if (isNull(receiverProvince)){
        swal('省份不能为空',{
            icon: "error"
        })
        return false;
    }

    var receiverCity = $('#receiver-city').val();
    if (isNull(receiverCity)){
        swal('城市不能为空',{
            icon: "error"
        })
        return false;
    }
    var receiverAddress = $('#receiver-address').val();
    if (isNull(receiverAddress)){
        swal('收货人地址不能为空',{
            icon: "error"
        })
        return false;
    }
    var receiverPhone = $('#receiver-phone').val();
    if (!validPhoneNumber(receiverPhone)){
        swal('收货人电话格式错误',{
            icon: "error"
        })
        return false;
    }

    var receiverZip = $('#receiver-zip').val();

    var address = {
        receiverName : receiverName,
        receiverProvince : receiverProvince,
        receiverCity : receiverCity,
        receiverAddress : receiverAddress,
        receiverPhone : receiverPhone,
        receiverZip : receiverZip
    }

    $.ajax({
        type: 'POST',
        url: '/address_add',
        contentType : 'application/json',
        data: JSON.stringify(address),
        success : function (result) {
            if (result.resultCode == 200){
                swal(result.message, {
                    icon: "success",
                });
                window.location.reload();
            }
        },
        error: function () {
            swal("操作失败", {
                icon: "error",
            });
        }
    })
})

// 修改收货地址
$(document).on('click','.address-btn-edit',function () {
    // 验证字段
    var receiverName = $('#receiver-name-edit').val();
    if (isNull(receiverName)){
        swal('收货人名称不能为空',{
            icon: "error"
        })
        return false;
    }

    var receiverProvince = $('#receiver-province-edit').val();
    if (isNull(receiverProvince)){
        swal('省份不能为空',{
            icon: "error"
        })
        return false;
    }

    var receiverCity = $('#receiver-city-edit').val();
    if (isNull(receiverCity)){
        swal('城市不能为空',{
            icon: "error"
        })
        return false;
    }
    var receiverAddress = $('#receiver-address-edit').val();
    if (isNull(receiverAddress)){
        swal('收货人地址不能为空',{
            icon: "error"
        })
        return false;
    }
    var receiverPhone = $('#receiver-phone-edit').val();
    if (!validPhoneNumber(receiverPhone)){
        swal('收货人电话格式错误',{
            icon: "error"
        })
        return false;
    }

    var receiverZip = $('#receiver-zip-edit').val();
    var id = $('#receiver-id-edit').val();

    var address = {
        receiverName : receiverName,
        receiverProvince : receiverProvince,
        receiverCity : receiverCity,
        receiverAddress : receiverAddress,
        receiverPhone : receiverPhone,
        receiverZip : receiverZip,
        id : id
    }

    $.ajax({
        type: 'POST',
        url: '/address_update',
        contentType : 'application/json',
        data: JSON.stringify(address),
        success : function (result) {
            if (result.resultCode == 200){
                alert("地址添加成功~");
                window.location.reload();
            }
        },
        error: function () {
            swal("操作失败", {
                icon: "error",
            });
        }
    })
})

// 删除地址
$(document).on('click','.address-delete',function () {
    if (window.confirm("确认删除该地址吗？")){
    var addressIdDel = $(this).parents('.address-item').data('id');
    $.ajax({
        type: 'POST',
        url: '/address_delete',
        data: { shippingId : addressIdDel},
        success : function (result) {
            if (result.resultCode == 200){
                window.location.reload();
            }
        },
        error: function () {
            swal("操作失败", {
                icon: "error",
            });
        }
    })
    }
})

var order_confirm = {
    data : {
        selectAddressId : null
    }
}
// 地址选中
$(document).on('click','.address-item',function () {
    $(this).addClass('active')
        .siblings('.address-item').removeClass('active');
    order_confirm.data.selectAddressId = $(this).data('id');
})

// 提交订单
$(document).on('click','.order-submit',function () {
    if (order_confirm.data.selectAddressId == null){
        swal("请选择地址再尝试提交~",{
            icon : "error"
        })
        return false;
    }
    $.ajax({
        type : 'POST',
        url : '/order-create',
        data : {shippingId : order_confirm.data.selectAddressId},
        success : function (result) {
            if (result.resultCode == 200){
                window.location.href = './payment.html?orderNumber='+result.data.orderNo;
            }else {
                swal(result.message,{
                    icon : "error"
                })
            }
        },
        error : function (result) {
            swal("操作错误----"+result.message,{
                icon : "error"
            })
        }
    })
})


