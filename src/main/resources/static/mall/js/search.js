$(function () {
        $('#search-input').keypress(function (e) {
        var key = e.which; //e.which是按键的值
        if (key == 13) {
            var q = $(this).val();
            if (q && q != '') {
                window.location.href = '/list?keyword=' + q;
            }
        }
    });
});

function search() {
    var q = $('#search-input').val();
    if (q && q != '') {
        window.location.href = '/list?keyword=' + q;
    }else{
        alert("请输入商品信息");
    }
}