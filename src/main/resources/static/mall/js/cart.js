$(function () {
    // 全选点击与反全选
    $(document).on('click','.cart-select-all',function () {
        var $this = $(this);
        if ($this.is(':checked')){
            $('.cart-select,.cart-select-all').prop('checked',true);
            $.ajax({
                type:'PUT',
                url:'/cart_select_all',
                success:function (res) {
                    if (res.resultCode == 200){
                        window.location.reload();
                    }
                },
                error:function () {
                    alert("哪里出错了？检查下吧！");
                }
            })
        }else {
            $('.cart-select,.cart-select-all').prop('checked',false);
            $.ajax({
                type:'PUT',
                url:'/cart_un_select_all',
                success:function (res) {
                    if (res.resultCode == 200){
                        window.location.reload();
                    }
                },
                error:function () {
                    alert("哪里出错了？检查下吧！");
                }
            })
        }
    });

    // 点击商品数量的增减
    $(document).on('click','.count-btn',function () {
        var $this = $(this),
            countType = $this.hasClass('plus') ? 'plus' : 'minus',
            $pCount = $this.siblings('.count-input'),
            currentCount = parseInt($pCount.val()),
            minCount = 1,
            maxCount = parseInt($pCount.data('max-stock')), // 传来的maxStock 会转换成 maxstock
            newCount = 0,
            productId = $this.parents('.cart-table').data('product-id');
        if (countType === 'plus'){
            if (currentCount >= maxCount){
                alert("已达商品最大数量，店小二正在紧张地去备货~");
                return;
            }
            newCount = currentCount + 1;
            $pCount.val(newCount);
        }else if (countType === 'minus'){
            if (currentCount <= minCount){
                return;
            }
            newCount = currentCount - 1;
            $pCount.val(newCount);
        }

        $.ajax({
            type:'PUT',
            url:'/cart_update',
            data:{
                newCount:newCount,
                productId:productId
            },
            success:function (res) {
                if (res.resultCode === 200){
                    window.location.reload();
                }else{
                    alert("操作商品数量失败~");
                }
            },
            error:function () {
                alert("好像哪里出错了！");
            }
        })
    })

    // 单个商品的删除
    $(document).on('click','.cart-delete',function () {
        if (window.confirm("确认删除该商品吗？")){
            var $this = $(this),
                productId = $this.parents('.cart-table').data('product-id');
            $.ajax({
                type:'DELETE',
                url:'/delete_product',
                data:{productIds:productId},
                success:function (res) {
                    if (res.resultCode == 200){
                        window.location.reload();
                    }
                },
                error:function () {
                    alert("好像哪里出错了！");
                }
            })
        }
    })

    // 多个商品的删除
    $(document).on('click','.delete-selected',function () {
        if (window.confirm("确认删除选中的商品？")){
            var arrayProductIds = [],
                $selectedItem = $('.cart-select:checked');
            // 循环查找选中的productIds
            for (var i = 0, iLength = $selectedItem.length;i < iLength;i++) {
                    arrayProductIds.push($($selectedItem[i]).parents('.cart-table').data('product-id'));
            }
            if (arrayProductIds.length){
                var productIds = arrayProductIds.join(',');
                $.ajax({
                    type:'DELETE',
                    url:'/delete_product',
                    data:{productIds:productIds},
                    success:function (res) {
                        if (res.resultCode == 200){
                            window.location.reload();
                        }
                    },
                    error:function () {
                        alert("好像哪里出错了！");
                    }
                })
            }else {
                alert("没有选择要删除的商品！");
            }
        }
    })

    // 提交购物车 span 不能用val()取里面的值。$(selector).attr(attribute)，attribute	规定属性的名称。
    $(document).on('click','.btn-submit',function () {
        if ( $('.cart-select:checked').length > 0 &&  $('.submit-total').attr("value") > 0){
            window.location.href = './order-confirm.html';
        }else {
            alert("请选择商品后再提交！");
        }
    })
});

// 只检测是否有未勾选和已经全部勾选这两个临界状态
function cartSelect(ths){
    // 判断是否已全选
    var checkLength = $('.cart-select:checkbox:checked').length;
    if (checkLength == $('.cart-select:checkbox').length){
        $('.cart-select-all').prop('checked',true);
    }

    // 判断是否单选修改后台数据库状态，并取消全选
    var $this = $(ths),
        productId = $this.parents('.cart-table').data('product-id');
    if (ths.checked == false){
        $.ajax({
            type:'PUT',
            data:{productId:productId},
            url:'/cart_un_select',
            success:function (res) {
                if (res.resultCode == 200){
                   window.location.reload();
                }
            },
            error:function () {
                alert("好像哪里出错了！");
            }
        })
        $('.cart-select-all').prop('checked',false);
    }
    if (ths.checked == true){
        $.ajax({
            type:'PUT',
            data:{productId:productId},
            url:'/cart_select',
            success:function (res) {
                if (res.resultCode == 200){
                    window.location.reload();
                }
            },
            error:function () {
                alert("好像哪里出错了！");
            }
        })
    }
}
