package com.why.biyesheji;

import com.why.biyesheji.controller.mall.UserController;
import com.why.biyesheji.pojo.User;
import com.why.biyesheji.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

@SpringBootTest
class BiyeshejiApplicationTests {


    @Test
    public void testFileUpload() throws FileNotFoundException {
        File path = new File(ResourceUtils.getURL("classpath:").getPath());
        System.out.println("path"+path);
        //文件上传的路径放入编译target目录下面 2、服务器的路径
        File upload = new File(path.getAbsolutePath(), "/static/upload");
        System.out.println("upload"+upload.toString());

    }

}
