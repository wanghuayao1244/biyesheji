package com.why.biyesheji;

import com.why.biyesheji.service.IShippingService;
import com.why.biyesheji.util.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by why on 2021/3/1
 **/
@SpringBootTest
public class TestShipping {

    @Autowired
    private IShippingService iShippingService;

    @Test
    public void testShippingDel(){
        Result result = iShippingService.addressDelete(2, 6);
        System.out.println(result.getMessage());
    }
}
