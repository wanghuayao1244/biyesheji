package com.why.biyesheji;

import com.github.pagehelper.PageInfo;
import com.why.biyesheji.pojo.Shipping;
import com.why.biyesheji.service.IShippingService;
import com.why.biyesheji.util.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Created by 华耀 王 on 2021/1/4
 **/
@SpringBootTest
public class TestOrder {

    @Autowired
    private IShippingService iShippingService;

    @Test
    public void TestShipping01(){
        Result result = iShippingService.shippingList(2,1,10);
        PageInfo pageInfo = (PageInfo) result.getData();
        List<Shipping> shippingList = pageInfo.getList();
        for (Shipping o : shippingList) {
            System.out.println("o = " + o);
        }
    }
}
