package com.why.biyesheji;

import com.why.biyesheji.pojo.Product;
import com.why.biyesheji.service.IProductService;
import com.why.biyesheji.util.Result;
import com.why.biyesheji.vo.ProductListVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Created by 华耀 王 on 2020/12/21
 **/
@SpringBootTest
public class TestProduct {

    @Autowired
    private IProductService iProductService;

    @Test
    public void test1(){
       Result result = iProductService.productList("拿铁", null, 1, 5, null);

    }

    @Test
    public void test2ProductDetail(){
        Result productDetail = iProductService.productDetail(2);
        System.out.println("product = " + productDetail.getData());
    }
}
