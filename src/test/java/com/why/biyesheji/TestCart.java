package com.why.biyesheji;

import com.why.biyesheji.pojo.Cart;
import com.why.biyesheji.service.ICartService;
import com.why.biyesheji.util.BigDecimalUtil;
import com.why.biyesheji.vo.CartListVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * Created by 华耀 王 on 2020/12/31
 **/
@SpringBootTest
public class TestCart {

    @Autowired
    private ICartService iCartService;

    @Test
    public void test01CartList(){
        CartListVO cartList = iCartService.cartList(2);
        System.out.println(cartList);
    }

    @Test
    void test02BigDecimal(){
        System.out.println(BigDecimalUtil.add(1.1111, 2.111));

    }

    @Test
    void test03selectAllChecked(){
        boolean t1 = iCartService.selectAllChecked(2);
        System.out.println("t1 = " + t1);
    }
}
